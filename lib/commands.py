# -*- coding: utf-8 -*-
import re

def parse_command(message):
    """
    :param message: str
    :return: комманду и агрументы
    :rtype: [str, str[]]
    """
    command = ''
    arguments = []

    message = message.strip()
    if message.startswith('!'):
        parserd = re.split('\s+', message)
        command = parserd[0][1:]
        arguments = parserd[1:]

    return command, arguments
