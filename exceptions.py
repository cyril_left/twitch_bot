from utils import choice_list


class RoundChoiceException(Exception):
    def __init__(self):
        Exception.__init__(self, 'Слишком мало вариантов!')


class RoundAlreadyExistsException(Exception):
    def __init__(self, cur_round):
        output = choice_list(cur_round)
        Exception.__init__(self, 'Уже есть существующая игра: {}'.format(output))
        self.output = output


class BelowZeroBetError(Exception):
    def __init__(self, value):
        Exception.__init__(self, 'Отдать денег вам не можем мы, должны положительное число вместо {} ввести Вы.'.format(value))
        self.count = value


class NullBetError(Exception):
    def __init__(self):
        Exception.__init__(self, 'Введена нулевая ставка. Ок, но зачем?')


class NoRunningBetsError(Exception):
    def __init__(self):
        Exception.__init__(self, 'На данный момент ставки не принимаются')


class BetClosedError(Exception):
    def __init__(self):
        Exception.__init__(self, 'Ставки закрыты!')


class NotEnoughMoneyError(Exception):
    def __init__(self, bet, user_money):
        Exception.__init__(self, 'Недостаточно денег, милорд: Вы пытаетесь поставить {}, а в казне всего {}'.format(bet, user_money))
        self.bet = bet
        self.user_money = user_money
