import random
import string

from settings import WHISPER, ADMINS



def random_util():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(4))


def whisper(line):
    return WHISPER + line


def user(line, user):
    return user + ' ' + line


def suffix(line):
    return line + '  ' + random_util()


def choice_list(cur_round):
    return '[' + ', '.join(str(key + 1) + ': ' + cur_round[key]['caption'] for key in cur_round) + ']'


def check_admins(username):
    if username not in ADMINS:
        return False
    else:
        return True