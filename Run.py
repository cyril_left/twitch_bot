"""
Simple IRC Bot for Twitch.tv

Developed by Aidan Thomson <aidraj0@gmail.com>
"""
import sys

import lib.irc as irc_
from lib.functions_general import *

from betBot import BetBot
from lib.commands import parse_command
from utils import *
from exceptions import *


class TwitchBot:

    def __init__(self, config):
        self.config = config
        self.irc = irc_.irc(config)
        self.socket = self.irc.get_irc_socket_object()

    def run(self):
        irc = self.irc
        sock = self.socket
        config = self.config
        betBot = BetBot(recover=True)

        while True:
            data = sock.recv(config['socket_buffer_size']).decode().rstrip()

            if len(data) == 0:
                pp('Connection was lost, reconnecting.')
                sock = self.irc.get_irc_socket_object()

            # if config['debug']:
            #     try:
            #         print(data)
            #     except Exception as e:
            #         print('error: {}'.format(e))

            # check for ping, reply with pong
            irc.check_for_ping(data)

            if irc.check_for_message(data):
                message_dict = irc.get_message(data)

                channel = message_dict['channel']
                message = message_dict['message']
                username = message_dict['username']
                admin = check_admins(username)
                ppi(channel, message, username)

                command, arguments = parse_command(message)
                try:
                    if command == 'exit':
                        if not admin:
                            irc.send_message(channel, whisper(user("¯\_(ツ)_/¯", username)))
                        else:
                            irc.send_message(channel, "Screw you guys, I'm going home!")
                            sys.exit(0)

                    elif command == 'богатеи':
                        if not admin:
                            irc.send_message(channel, whisper(user("¯\_(ツ)_/¯", username)))
                        else:
                            rich_asses = betBot.get_top_user_money(arguments)
                            #print(rich_asses)
                            message = ' deIlluminati '.join('{}: {}'.format(rich, betBot.get_users()[rich].get_money()) for rich in rich_asses)
                            print(message)
                            irc.send_message(channel, whisper(user(message, username)))
                    
                    elif command == 'раунд':
                        if not admin:
                            irc.send_message(channel, whisper(user("¯\_(ツ)_/¯", username)))
                        else:
                            try:
                                result = betBot.create_round(arguments)
                                message = suffix("Ставки на {} открыты".format(choice_list(result)))

                            except (RoundChoiceException, RoundAlreadyExistsException) as ex:
                                message = whisper(user(str(ex), username))
                            print(message)
                            irc.send_message(channel, message)

                    elif command == 'ставлю':
                        team = arguments[0]
                        bet = arguments[1]
                        try:
                            inner_team = int(team) - 1
                        except:
                            inner_team = team
                        try:
                            answer_team = betBot.accept_bet(username, bet, inner_team)
                            message = whisper(user("ставка {} на {} принята".format(bet, answer_team), username))
                        except ValueError as ex:
                            correct_choices = choice_list(betBot.current_round.get_choices())
                            message = whisper(user('Вы пытаетесь поставить на {}, но такой ставки не существует! NotLikeThis Вот какие варианты я могу предложить взамен: {}'.format(team, correct_choices), username))
                        except (ValueError,
                                BelowZeroBetError,
                                NullBetError,
                                NoRunningBetsError,
                                BetClosedError,
                                NotEnoughMoneyError) as ex:
                            print(message)
                            message = whisper(user(str(ex), username))
                        print(message)
                        irc.send_message(channel, message)

                    elif command == 'закрытьСтавки':
                        if not admin:
                            irc.send_message(channel, whisper(user("¯\_(ツ)_/¯", username)))
                        else:
                            betBot.close_bets()
                            message = suffix("Ставки закрыты!")
                            print(message)
                            irc.send_message(channel, message)

                    elif command == 'выиграл':
                        if not admin:
                            irc.send_message(channel, whisper(user("¯\_(ツ)_/¯", username)))
                        else:
                            team = arguments[0]
                            try:
                                inner_team = int(team) - 1
                            except:
                                inner_team = team
                            winners, answer_team = betBot.close_round(inner_team)
                            message = suffix("Round win {}".format(answer_team))
                            irc.send_message(channel, message)
                            for winner in winners:
                                if winners[winner]['income'] == 0:
                                    income = 'проиграли'
                                else:
                                    income = 'Выиграли {}'.format(winners[winner]['income'])
                                message = whisper(user('Победила команда {}! Вы {}. Ваш баланс: {}'.format(answer_team, income, winners[winner]['money']), winner))
                                irc.send_message(channel, message)

                    elif command == 'деньги':
                        money = betBot.get_user_money(username)
                        message = whisper(user("Ваши деньги: {}".format(money), username))
                        print(message)
                        irc.send_message(channel, message)

                    elif command == 'ставки':
                        try:
                            team = [arguments[0], ]
                            try:
                                team = [int(team[0]) - 1, ]
                            except:
                                pass
                        except:
                            team = None
                        try:
                            rates = betBot.get_rates(username, team)
                            if team:
                                rate = list(rates.values())[0]
                                answer = 'Коэффициент на команду {} составляет {} (Вы поставили {})'.format(rate['caption'],
                                                                                                             rate['rate'],
                                                                                                             rate['my_bet'])
                                message = whisper(user(answer, username))
                            else:
                                answer = ' BudBlast '.join("{} // {} // {}".format(rates[rate]['caption'],
                                                                                  rates[rate]['rate'],
                                                                                  rates[rate]['my_bet'])
                                                          for rate in rates)
                                message = whisper(user("Текущие коэффициенты в виде название команды // коэффициент // моя ставка:  {}".format(answer), username))
                        except NoRunningBetsError as ex:
                            message = whisper(user(str(ex), username))
                        irc.send_message(channel, message)

                    else:
                        message = whisper(user('к сожалению, я не знаю, что такое "{}", попробуйте что-нибудь ещё', username))
                except Exception as e:
                    print('###Error: ' + str(e))
                    message = whisper(user('что-то пошло не так!', username))
                    print(message)
                    irc.send_message(channel, message)


if __name__ == "__main__":
    from lib.config import CONFIG

    TwitchBot(CONFIG).run()
