from __future__ import division
# -*- coding: utf-8 -*-
import pytest
from betBot import Round, BetBot

class TestRoundClass:
    def setup_method(self, test_method):
        self.round = Round(['one', 'two'])

    def teardown_method(self, test_method):
         self.round = None

    def test_NotAcceptBetsAfterClose(self):
        self.round.close_bets()
        assert self.round.ACCEPT_BETS is False

    def test_AcceptBetsAfterCreation(self):
        assert self.round.ACCEPT_BETS is True

    def testAcceptBet(self):
        self.round.accept_bet('usename', 300, 'one')
        assert self.round.bet_choices[0]['total_sum'] == 300

    def testAcceptBetNoChoice(self):
        with pytest.raises(Exception):
            self.round.accept_bet('usename', 300, 'gray')

    def testGetRates(self):
        self.round.accept_bet('usename', 300, 'one')
        self.round.accept_bet('usename', 300, 'two')
        self.round.accept_bet('usename', 300, 'two')
        self.round.accept_bet('usename', 300, 'two')
        result = self.round.get_rates()
        assert result[0]['rate'] == 4


class TestBetBotSimpleClass:
    def setup_method(self, test_method):
        self.betbot = BetBot()

    def teardown_method(self, test_method):
        self.betbot = None

    def testCreateUser(self):
        self.betbot.create_user('first')
        assert 'first' in self.betbot.user_pool
        assert self.betbot.user_pool['first'].money_in_the_pocket == 500

    def testSetGetCasinoCash(self):
        self.betbot._set_casino_cash(500)
        assert self.betbot.get_casino_cash() == 500

    def testCreateWrongRound(self):
        with pytest.raises(Exception):
            self.betbot.create_round(['one'])

    def testCreateMultipleRounds(self):
        self.betbot.create_round(['one', 'two'])
        with pytest.raises(Exception):
            self.betbot.create_round(['three', 'four'])

    def testAcceptBetWrongBet(self):
        self.betbot.create_round(['one', 'two'])
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', 'MILLION', 'one')

    def testAcceptBetWrongChoice(self):
        self.betbot.create_round(['one', 'two'])
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', '100', 'three')

    def testAcceptBetZero(self):
        self.betbot.create_round(['one', 'two'])
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', 0, 'one')

    def testAcceptBetNegative(self):
        self.betbot.create_round(['one', 'two'])
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', -100, 'one')

    def testAcceptBetNoRounds(self):
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', '100', 'one')

    def testAcceptBetClosedRound(self):
        self.betbot.create_round(['one', 'two'])
        self.betbot.close_bets()
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', '100', 'one')

    def testAcceptBetNotEnoughMoney(self):
        self.betbot.create_round(['one', 'two'])
        with pytest.raises(Exception):
            self.betbot.accept_bet('tester', '600', 'three')

    def testRoundRussianChoices(self):
        self.betbot.create_round(['один', 'два'])
        self.betbot.accept_bet('tester', '100', 'один')
        self.betbot.accept_bet('tester2', '300', 'два')
        self.betbot.close_round('один')
        money = self.betbot.user_pool['tester'].money_in_the_pocket
        assert money == 800

class TestBetBotRatesClass():
    def setup_method(self, test_method):
        self.betbot = BetBot()
        self.bets = {
            'username': 150,
            'username1': 100,
            'username2': 200,
            'username3': 500,
        }
        self.betbot.create_round(['one', 'two'])


    def teardown_method(self, test_method):
        self.betbot = None

    def testRatesSolo(self):
        self.betbot.accept_bet('username', self.bets['username'], 'one')


        self.betbot.close_bets()
        assert self.betbot.current_round.get_rates() ==  {0: {'caption': 'one', 'rate': 1.0}, 1: {'caption': 'two', 'rate': 1.0}}

    def testRatesMany(self):
        self.betbot.accept_bet('username', self.bets['username'], 'one')
        self.betbot.accept_bet('username1', self.bets['username1'], 'two')
        self.betbot.accept_bet('username2', self.bets['username2'], 'two')

        self.betbot.close_bets()
        assert self.betbot.current_round.get_rates() ==  {0: {'caption': 'one', 'rate': 3.0}, 1: {'caption': 'two', 'rate': 1.5}}

class TestBetBotComplexClass():
    def setup_method(self, test_method):
        self.betbot = BetBot()
        self.start_money = 500
        self.bets = {
            'username': 150,
            'username1': 100,
            'username2': 200,
            'username3': 500,
        }
        self.betbot.create_round(['one', 'two'])
        self.betbot.accept_bet('username', self.bets['username'], 'one')
        self.betbot.accept_bet('username1', self.bets['username1'], 'two')
        self.betbot.accept_bet('username2', self.bets['username2'], 'two')
        self.betbot.accept_bet('username3', self.bets['username3'], 'two')

        self.betbot.close_bets()
        self.betbot.close_round('one')

    def teardown_method(self, test_method):
        self.betbot = None

    def testCloseRoundWinner(self):
        # pot = 945
        winner_bet = self.bets['username']
        pot = sum(self.bets.values())
        pot_without_winner = pot - winner_bet
        win_money = (int(((pot_without_winner / winner_bet) + 1) * 10) / 10) * winner_bet
        # 945 = о((800 / 150) + 1) * 150
        # где о - округление до десятых в меньшую сторону, выполняемое через int(x*10)/10
        winner_pocket = self.betbot.user_pool['username'].money_in_the_pocket
        not_played_money = (self.start_money - winner_bet)

        assert winner_pocket == (win_money + not_played_money)

    def testCloseRoundCasinoCash(self):
        assert self.betbot.casino_cash == 5

    def testMoneySum(self):
        """Кол-во денег в игре должно быть не изменно после закрытия игры."""
        sumUsersMoney = sum(map(lambda x: x.money_in_the_pocket, self.betbot.user_pool.values()))
        mustBeMoneyInGame = len(self.bets.values()) * self.start_money
        assert sumUsersMoney + 5 == mustBeMoneyInGame

    def testCloseRoundLooser1(self):
        money = self.betbot.user_pool['username1'].money_in_the_pocket
        assert money == self.start_money - self.bets['username1']

    def testCloseRoundLooser3(self):
        money = self.betbot.user_pool['username3'].money_in_the_pocket
        assert money == self.start_money - self.bets['username3']


class TestBetBotRecoverClass():
    def setup_method(self, test_method):
        self.betbot = BetBot()

    def teardown_method(self, test_method):
        self.betbot = None

    def testRecoverBackup(self):
        self.betbot = self.betbot.recover_backup('test_recover.testcsv')
        assert self.betbot.user_pool['username2'].money_in_the_pocket == 300
