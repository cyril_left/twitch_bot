# -*- coding: utf-8 -*-
import pytest
from lib.commands import parse_command

class TestParseCommand:

    def test_getCommand(self):
        command, arguments = parse_command('!test')
        assert command == 'test'

    def test_getCommandTrim(self):
        command, arguments = parse_command('   !test')
        assert command == 'test'

    def test_getCommandTrimRight(self):
        command, arguments = parse_command('!test  ')
        assert command == 'test'

    def test_getArguments(self):
        command, arguments = parse_command('!test arg1 arg2')
        assert arguments == ['arg1', 'arg2']

    def test_getArgumentsRussian(self):
        command, arguments = parse_command('!test нахер иди')
        assert arguments == ['нахер', 'иди']
