# -*- coding: utf-8 -*-
from __future__ import division

import os
import datetime
import pickle
from settings import *
from exceptions import *
import requests


class User():
    """класс описывающий юзера и его денежки"""

    def __init__(self, user_name, money):
        self.username = user_name
        self.money_in_the_pocket = money

    def get_money(self):
        return round(self.money_in_the_pocket, 2)

    def set_money(self, money):
        self.money_in_the_pocket = money

    def add_money(self, money):
        self.money_in_the_pocket += money

    def decrease_money(self, money):
        self.money_in_the_pocket -= money


class BetBot():
    """класс api открытия/закрытия/приёма ставок"""

    def __init__(self, recover=False):
        self.user_pool = dict()
        self.casino_cash = 0
        self.current_round = False
        if recover:
            self.recover_backup(recover)

    def _isstr(self, s):
        try:
            return isinstance(s, basestring)
        except NameError:
            return isinstance(s, str)

    def recover_backup(self, recover, archive_dir=ARCHIVE_DIR):
        if not self._isstr(recover):
            try:
                recover = max(os.listdir(archive_dir), key=os.path.getctime)
            except:
                return
        with open(archive_dir + recover, 'rb') as source:
            return pickle.load(source)

    def _backup(self, archive_dir=ARCHIVE_DIR):
        name_mask = datetime.datetime.now().strftime('%Y.%m.%d__%H-%M-%S.bckp')
        # pickle.dump(self, archive_dir + name_mask)
        with open(archive_dir + name_mask, 'wb') as target:
            pickle.dump(self, target)

    def _set_casino_cash(self, cash):
        self.casino_cash += cash

    def get_casino_cash(self):
        return self.casino_cash

    def create_user(self, user_name, money=MONEY):
        """создаём юзера"""
        self.user_pool[user_name] = User(user_name=user_name, money=money)
        return True

    def create_round(self, choices):
        """создаём очередную итерацию игры"""
        if len(choices) < 2:
            raise RoundChoiceException()
        if not self.current_round:
            self.current_round = Round(choices)
            self.current_round.post_rates()
            return self.current_round.get_choices()
        else:
            raise RoundAlreadyExistsException(self.current_round.get_choices())    

    def accept_bet(self, user_name, bet_sum, bet_choice):
        """Принимаем ставку, вычетаем из кошелька"""
        try:
            bet_sum = int(bet_sum)
        except ValueError:
            raise
        if bet_sum < 0:
            raise BelowZeroBetError(bet_sum)
        if bet_sum == 0:
            raise NullBetError()
        if not self.current_round:
            raise NoRunningBetsError()
        if not self.current_round.get_bet_acceptance():
            raise BetClosedError()
        if user_name not in self.user_pool:
            self.create_user(user_name)
        if self.user_pool[user_name].get_money() < bet_sum:
            raise NotEnoughMoneyError(bet_sum,
                                      self.user_pool[user_name].get_money())
        
        answer = self.current_round.accept_bet(user_name,
                                      bet_sum,
                                      bet_choice)
        self.user_pool[user_name].decrease_money(bet_sum)
        #print('sending')
        self.current_round.post_rates()
        #print('sent')
        return answer

    def close_bets(self):
        """закрываем приняте ставок"""
        self._backup()
        self.current_round.close_bets()
        r = requests.post('http://ep1c.org:3000/api/bets', json={'message': dict()})

    def open_bets(self):
        """открываем принятие ставок (по дефолту открыты)"""
        self.current_round.open_bets()

    def close_round(self, winner):
        """закрываем игру, раздаём деньги, отстёгиваем себе процент"""
        winners = dict()
        for user in self.get_users():
            winners[user] = {'money': self.user_pool[user].get_money(),
                             'income': 0}
        winner = self.current_round.check_choice(winner)
        win_rates = self.current_round.get_rates(None, [winner, ], False)[winner]
        for user in self.current_round.get_choices()[winner]['bets']:
            gain = user['bet'] * (int((win_rates['rate']) * 10) / 10)
            casino_gain = (user['bet'] * win_rates['rate']) - gain
            self._set_casino_cash(casino_gain)
            self.user_pool[user['user']].add_money(gain)
            winners[user['user']]['income'] = gain
            winners[user['user']]['money'] = self.user_pool[user['user']].get_money()
        self._backup()
        team = self.current_round.bet_choices[winner]['caption']
        self.current_round = False
        return winners, team

    def get_user_money(self, user_name):
        if user_name not in self.user_pool:
            self.create_user(user_name)
        return self.user_pool[user_name].get_money()

    def get_users(self):
        return self.user_pool

    def get_top_user_money(self, top=3):
        try:
            top = int(top)
        except:
            top = 3
        if top < 0:
            top = abs(top)
        if top == 0:
            top = 3
        return sorted(self.get_users(), key=lambda x: self.user_pool[x].get_money(), reverse=True)[:top]

    def get_rates(self, user_name=None, choices=None, showoff=True):
        if not self.current_round:
            raise NoRunningBetsError()
        return self.current_round.get_rates(user_name, choices, showoff)


class Round():
    def __init__(self, choices=DEFAULT_CHOICES):
        self.ACCEPT_BETS = True
        self.raw_choices = choices
        self.bet_choices = dict()
        for key, caption in enumerate(choices):
            self.bet_choices[key] = {
                'bets': [],
                'total_sum': 0,
                'caption': caption
            }

    def check_choice(self, bet_choice):

        if bet_choice not in self.bet_choices:
            try:
                return self.raw_choices.index(bet_choice)
            except ValueError:
                raise
        else:
            return bet_choice

    def accept_bet(self, user_name, bet_sum, bet_choice):
        bet_choice = self.check_choice(bet_choice)
        self.bet_choices[bet_choice]['bets'].append({'user': user_name,
                                                     'bet': bet_sum})
        self.bet_choices[bet_choice]['total_sum'] += bet_sum
        answer = self.bet_choices[bet_choice]['caption']
        return answer

    def get_rates(self, user_name=None, choices=None, showoff=True):
        rates = dict()
        if choices is None:
            choices = self.bet_choices
        for choice in choices:
            choice = self.check_choice(choice)
            other_choices = sum(self.bet_choices[other_choice]['total_sum']
                                for other_choice in self.bet_choices
                                if not other_choice == choice)
            current_choice = self.bet_choices[choice]['total_sum']
            if current_choice == 0:
                current_choice = 1
                other_choices = 0
            rates[choice] = {'rate': (1 + other_choices / float(current_choice))}
            if showoff:
                rates[choice]['rate'] = int(rates[choice]['rate'] * 10) / 10
            rates[choice]['caption'] = self.bet_choices[choice]['caption']
            if user_name:
                rates[choice]['my_bet'] = sum(my_choice['bet']
                                              for my_choice in self.bet_choices[choice]['bets']
                                              if my_choice['user'] == user_name)

        return rates

    def post_rates(self):
        rates = dict()
        text = ''
        choices = self.bet_choices
        for choice in choices:
            choice = self.check_choice(choice)
            other_choices = sum(self.bet_choices[other_choice]['total_sum']
                                for other_choice in self.bet_choices
                                if not other_choice == choice)
            current_choice = self.bet_choices[choice]['total_sum']
            if current_choice == 0:
                current_choice = 1
                other_choices = 0
            rates[choice] = {'rate': (1 + other_choices / float(current_choice))}
            rates[choice]['rate'] = int(rates[choice]['rate'] * 10) / 10
            rates[choice]['caption'] = self.bet_choices[choice]['caption']
            rates[choice]['betssum'] = sum(my_choice['bet']
                                           for my_choice in self.bet_choices[choice]['bets'])
            rates[choice]['betscount'] = len(self.bet_choices[choice]['bets'])
            #text += '<br/>' + rates[choice]['caption'] + ' | ' + str(rates[choice]['rate']) + ' | ' + str(rates[choice]['betscount'])  + '|' + str(rates[choice]['betssum'])
        message = {'message': rates}
        #message = {'message': text}
        r = requests.post('http://ep1c.org:3000/api/bets', json=message)
        print(r)
        return

    def get_choices(self):
        return self.bet_choices

    def close_bets(self):
        self.ACCEPT_BETS = False

    def open_bets(self):
        self.ACCEPT_BETS = True

    def get_bet_acceptance(self):
        return self.ACCEPT_BETS
